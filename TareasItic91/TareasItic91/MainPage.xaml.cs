﻿using Android.Provider;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TareasItic91.Data;
using Xamarin.Forms;

namespace TareasItic91
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {

        private IList<Tarea> tareas = new ObservableCollection<Tarea>();
        private TareaManager manager = new TareaManager();
       
        public MainPage()
        {
            BindingContext = tareas;
            InitializeComponent();
        }

        async void OnRefresh(object sender, EventArgs e)
        {
            var tareasCollection = await manager.GetAll();
             foreach (Tarea tarea in tareasCollection)
                {
                if(tareas.All(t=> t.id != tarea.id) )
                    tareas.Add(tarea);
                }

        }
        async private void OnAddTarea(object sender, EventArgs e)
        {
           await Navigation.PushModalAsync(new AddTarea(manager));
        }
        //async private void OnEditTarea(object sender, EventArgs e)
        //{
        //    await Navigation.PushModalAsync( EditTarea());
        //}

        private void OnDeleteTarea(object sender, EventArgs  e)
        {
            //var item = (Contacts)e.SelectedItem;
            //i)
            const string url = "http://192.168.8.5:3000/tareas";

            HttpClient client = new HttpClient();
            //string urldelete = url + "/" + id;
        }
        //async private void OnDeleteTarea(object sender, EventArgs e)
        //{
        //    await Navigation.PushModalAsync(manager.DeleteTarea);
        //}



    }
}
